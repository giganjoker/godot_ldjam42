extends KinematicBody2D

const UP = Vector2(0,-1)

const GRAVITY = 10
const GLIDE_GRAVITY = 2

var g = GRAVITY

var gliding
export(float) var maxGlidingYSpeed = 30

export(int) var JUMP_HEIGHT = 500

export(int) var mySpeed = 50
export(int) var goalSpeed = 1000
export(int) var minimumSpeed = 0
var motion = Vector2()

var shield = false

var runningAudio
var umbrellaAudio
var umbrellaPlayed = false


var shieldTimer

func _ready():
	JUMP_HEIGHT *= -1
	
	runningAudio = get_tree().get_root().find_node("SFX", true, false).find_node("Run")
	umbrellaAudio = get_tree().get_root().find_node("SFX", true, false).find_node("UmbrellaFloat")
	
	
	shieldTimer = Timer.new()
	shieldTimer.connect("timeout", self, "shieldOver")
	add_child(shieldTimer)
	
func _process(delta):
	if is_on_floor():
		if Input.is_action_just_pressed("space"):
			motion.y = JUMP_HEIGHT
	
	#if falling downa and space pressed
	if motion.y > 0 && Input.is_action_pressed("space"):
		g = GLIDE_GRAVITY
		gliding = true
	else:
		g = GRAVITY
		gliding = false
		
		
	if is_on_floor():
		playAnimation("Running")
		if not runningAudio.is_playing():
			runningAudio.play()
			print("yo")
	else:
		if motion.y > 0 && Input.is_action_pressed("space"):
			playAnimation("UmbrellaOpening")
			
			if not umbrellaPlayed:
				umbrellaAudio.play()
				umbrellaPlayed= true
		else:
			playAnimation("InAir")
			umbrellaPlayed = false
			
		if runningAudio.is_playing():
			runningAudio.stop()
			
	#mySpeed = 900
	#goal
	if mySpeed >= goalSpeed:
		get_tree().change_scene("res://scenes/EndScreen/EndScreen.tscn")
				
var playingAnimation = ""
func playAnimation(name):
	if playingAnimation != name:
		$AnimationPlayer.play(name)
		playingAnimation = name
			

func _physics_process(delta):
	motion.y += g
	motion.x = mySpeed
	
	#print(motion.y)
	
	if gliding:
		motion.y = min(motion.y, maxGlidingYSpeed)
	
	motion = move_and_slide(motion, UP)
	
func decreaseSpeed(speed):
	if shield:
		return
	
	mySpeed -= speed
	print("ouch")
	
	if mySpeed <= minimumSpeed:
		get_tree().reload_current_scene()
	
func increaseSpeed(speed):
	mySpeed += speed
	
func useShield(time):
	if not shieldTimer.is_stopped():
		shieldTimer.stop()
	
	shieldTimer.set_wait_time(time)
	shieldTimer.start()
	shield = true;
	
	
	
func shieldOver():
	print("shield is down")
	shield = false
	