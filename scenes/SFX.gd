extends Node

#		get_tree().get_root().find_node("SFX", true, false).find_node("Run").play()


onready var Gunshots = [$GunShot1, $GunShot2, $GunShot3, $GunShot4]
onready var MobSpeaks = [$GunCocking, $MobSpeak1, $MobSpeak2, $MobSpeak3, $MobSpeak4, $MobSpeak5, $MobSpeak6]
onready var ObstacleFalls = [$ObstacleFall1, $ObstacleFall2]
onready var ObstacleHits = [$ObstacleHit1, $ObstacleHit2, $ObstacleHit3]

onready var ObstacleWoods = [$ObstacleWood1, $ObstacleWood2, $ObstacleWood3]

func playGunShot():
	Gunshots[int(rand_range(0,Gunshots.size()))].play()

func playMobSpeak():
	MobSpeaks[int(rand_range(0,MobSpeaks.size()))].play()
	
func playObstacleHit():
	ObstacleHits[int(rand_range(0,ObstacleHits.size()))].play()
	
		
func playObstacleFall():
	ObstacleFalls[int(rand_range(0,ObstacleFalls.size()))].play()
	print("yo")
	
func playObstacleWood():
	ObstacleWoods[int(rand_range(0,ObstacleWoods.size()))].play()