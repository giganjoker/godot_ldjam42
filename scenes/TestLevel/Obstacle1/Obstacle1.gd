extends Area2D

enum SoundType{
	hit, wood, fall
}
export(int) var decreaseSpeedBy = 10
export(SoundType) var sound

func _on_Obstacle1_body_entered(body):
	if body.name == "Hero":
		body.decreaseSpeed(decreaseSpeedBy)
		
		if sound == SoundType.hit:
			get_tree().get_root().find_node("SFX", true, false).playObstacleHit()
		elif sound == SoundType.wood:
			get_tree().get_root().find_node("SFX", true, false).playObstacleWood()
		elif sound == SoundType.fall:
			get_tree().get_root().find_node("SFX", true, false).playObstacleFall()
			print("fall")