extends Area2D

export(int) var increaseSpeedBy = 0

func _on_Obstacle2_body_entered(body):
	if body.name == "Hero":
		get_tree().get_root().find_node("SFX", true, false).find_node("ItemSpeed").play()
		body.increaseSpeed(increaseSpeedBy)
		queue_free()