extends Area2D

export(int) var decreaseSpeedBy = 0
export(int) var bulletSpeed = 0
export(int, -1, 1) var direction = -1;

func init(decreaseSpeedBy, bulletSpeed, direction):
	self.decreaseSpeedBy = decreaseSpeedBy
	self.bulletSpeed = bulletSpeed
	self.direction = direction

func _ready():
	bulletSpeed *= direction
	scale.x *= direction

func _physics_process(delta):
	position.x += bulletSpeed * delta
	
	if global_position.x < -get_viewport().get_canvas_transform()[2].x:
		queue_free()
	
func _on_Obstacle2_body_entered(body):
	if body.name == "Hero":
		get_tree().get_root().find_node("SFX", true, false).playObstacleHit()
		body.decreaseSpeed(decreaseSpeedBy)
		queue_free()
