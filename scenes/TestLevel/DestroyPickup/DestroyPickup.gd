extends Area2D

func _on_Obstacle2_body_entered(body):
	if body.name == "Hero":
		var enemies = get_tree().get_nodes_in_group("obstacle")
		get_tree().get_root().find_node("SFX", true, false).find_node("ItemDestroy").play()
		for e in enemies:
			e.queue_free()
		queue_free()