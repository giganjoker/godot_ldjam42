extends Area2D

export(int) var decreaseSpeedBy = 0
export(int) var bulletDecSpeed = 0
export(int) var bulletSpeed = 0
export(float) var shootFrequency = 1
export(int, -1, 1) var direction = -1
export(Resource) var bulletScene
export(float) var startShootRange = 370

var timer

var hero

func _ready():
	scale.x *= direction
	timer = Timer.new()
	timer.set_wait_time(1/shootFrequency)
	timer.connect("timeout", self, "shoot")
	add_child(timer)
	
	
	hero = get_tree().get_root().find_node("Hero", true, false)
	
func _physics_process(delta):
	if hero != null:
		if timer.is_stopped() && startShootRange > global_position.distance_to(hero.global_position):
			timer.start()
			get_tree().get_root().find_node("SFX", true, false).playMobSpeak()
			print("start shooting")
			

func _on_Obstacle3_body_entered(body):
	if body.name == "Hero":
		body.decreaseSpeed(decreaseSpeedBy)
		get_tree().get_root().find_node("SFX", true, false).playObstacleHit()

func shoot():
	get_tree().get_root().find_node("SFX", true, false).playGunShot()
	var newBullet = bulletScene.instance()
	newBullet.init(bulletDecSpeed, bulletSpeed, 1) 
	newBullet.set_position($shootpoint.get_position())
	newBullet.find_node("Sprite").set_z_index($Sprite.z_index)
	add_child(newBullet)

