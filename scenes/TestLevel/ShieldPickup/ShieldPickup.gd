extends Area2D

export(float) var shieldTime = 3

func _on_Obstacle2_body_entered(body):
	if body.name == "Hero":
		get_tree().get_root().find_node("SFX", true, false).find_node("ItemShield").play()
		body.useShield(shieldTime)
		queue_free()