extends Node

func _ready():
	$Node2D/AnimationPlayer.play("float around")

func _on_playagain_pressed():
	get_tree().change_scene("res://scenes/madeby.tscn")

func _on_quit_pressed():
	get_tree().quit()
	
func _input(event):
	if (event.is_pressed() and event.button_index == BUTTON_LEFT):
    	$ClickAudio.play()