extends Camera2D

var speed = 10
export(int) var acc = 20

func _physics_process(delta):
	speed += acc
	position.x += speed*delta

func _on_Timer_timeout():
	get_tree().change_scene("res://scenes/EndScreen/OutOfSpace.tscn")
