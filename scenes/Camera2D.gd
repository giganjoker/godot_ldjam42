extends Camera2D

var hero
var yPos
var xOffset
var yOffset

func _ready():
	hero = get_tree().get_root().find_node("Hero", true, false)
	xOffset = position.x - hero.get_position().x
	yPos = position.y

func _process(delta):
	set_position(Vector2(hero.get_position().x, yPos))