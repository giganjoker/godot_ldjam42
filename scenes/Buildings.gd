extends Node

var ySpawn
var xPos

var LIMIT = 10

export(Array) var buildings

func _ready():
	ySpawn = $SpawnPointY.position.y
	xPos = $SpawnPointY.position.x
	
	for i in range(0, LIMIT):
		spawn()
	
func spawn():
	var rand = int(rand_range(0,buildings.size()))
	var spawn = buildings[rand].instance()
	xPos += spawn.find_node("Sprite").get_position().x*-2
	spawn.set_position(Vector2(xPos, ySpawn))
	add_child(spawn)
