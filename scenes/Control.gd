extends Control

onready var hero = get_tree().get_root().find_node("Hero", true, false)

func _process(delta):
	$speedhbox/Speed.text = "Speed:"+str(hero.mySpeed)+"/"+str(hero.goalSpeed)
	
	$defensehbox/Sprite.set_visible(hero.shield)