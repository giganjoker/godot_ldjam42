# MainMenu.gd
extends Control

func _ready():
	$Animation/AnimationPlayer.play("run")
	$Animation/SpaceShipRock.play("rock")

func _on_Start_pressed():
	get_tree().change_scene("res://scenes/scene1.tscn")


func _on_Quit_pressed():
	get_tree().quit()


func _on_Help_pressed():
	$main.visible = false
	$help.visible = true


func _on_hide_pressed():
	$main.visible = true
	$help.visible = false


func _input(event):
	if (event.is_pressed() and event.button_index == BUTTON_LEFT):
    	$ClickAudio.play()
