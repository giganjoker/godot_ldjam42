extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(3)
	timer.connect("timeout",self,"on_timeout")
	add_child(timer)
	timer.start()
	
	pass

func on_timeout():
	get_tree().change_scene("res://scenes/mainmenu.tscn")

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
