extends Node
	

#var tileSpawnableClass = preload("res://Class/TileSpawnable.gd")

const LIMIT = 65
var current = 1

var baseLandPos = Vector2()
export(float) var landWidth
export(Resource) var landScene

export(Array) var spawnables
var spawnableRNGBounds = Array()



export(int) var spawnChance

export(Vector2) var minSpawnXSpeedYChance = Vector2(300,5);
export(Vector2) var maxSpawnXSpeedYChance = Vector2(1000,15);

var xSlopeYConstant = Vector2()

var ratioNumberTotal = 0.0

onready var hero = get_tree().get_root().find_node("Hero", true, false)

func _ready():
	baseLandPos = $land.position
		
	for s in spawnables:
		ratioNumberTotal += s.ratioNumber
		print(s.ratioNumber)
		spawnableRNGBounds.append(ratioNumberTotal)
		
	for i in range(0, LIMIT):
		generateNew(false)
		
	xSlopeYConstant.x = (maxSpawnXSpeedYChance.y - minSpawnXSpeedYChance.y)/(maxSpawnXSpeedYChance.x - minSpawnXSpeedYChance.x)
	xSlopeYConstant.y = ((maxSpawnXSpeedYChance.x * minSpawnXSpeedYChance.y) - (maxSpawnXSpeedYChance.y * minSpawnXSpeedYChance.x))/(maxSpawnXSpeedYChance.x - minSpawnXSpeedYChance.x)
	#print(xSlopeYConstant)
	print(spawnableRNGBounds)
	

func generateNew(spawnObjects = true):
	var newLand = landScene.instance()
	newLand.set_position(Vector2(baseLandPos.x+(landWidth*current), baseLandPos.y))
	add_child(newLand)
	current+=1
	
	if !spawnObjects:
		return
	
	if hero.mySpeed <= minSpawnXSpeedYChance.x:
		spawnChance = minSpawnXSpeedYChance.y
	elif hero.mySpeed >= maxSpawnXSpeedYChance.x:
		spawnChance = maxSpawnXSpeedYChance.y
	else:
		spawnChance = xSlopeYConstant.x * hero.mySpeed + xSlopeYConstant.y
	
	#print(spawnChance)
	
	#will spawn an object on tile?
	randomize()
	var toSpawnRand = rand_range(0, spawnChance);
	var toSpawn = (toSpawnRand <= 1) 
	
	
	if toSpawn:
		#what to spawn on the tile
		randomize()
		var rand = rand_range(0, ratioNumberTotal)
		
		for i in range(0, len(spawnables)):
			if rand < spawnableRNGBounds[i]:
				print(str(rand)+","+str(spawnableRNGBounds[i])+","+str(i))
				#spawnobjects
				var spawn = spawnables[i].object.instance()
				
				if spawnables[i].doesTakeBothLanes:
					spawn.set_position(newLand.find_node("SpawnPointDown").position)
				else:
					#spawnloc
					randomize()
					var spawnPosRand = int(rand_range(0,10))
					if spawnPosRand < 4:
						spawn.set_position(newLand.find_node("SpawnPointUp").position)
						spawn.find_node("Sprite").set_z_index(10)
					elif spawnPosRand < 8:
						spawn.set_position(newLand.find_node("SpawnPointDown").position)
						spawn.find_node("Sprite").set_z_index(20)
					else:
						spawn.set_position(newLand.find_node("SpawnPointUp").position)
						spawn.find_node("Sprite").set_z_index(10)
						var spawn2 = spawn.duplicate()
						spawn2.set_position(newLand.find_node("SpawnPointDown").position)
						spawn2.find_node("Sprite").set_z_index(20)
						newLand.add_child(spawn2)
				
				newLand.add_child(spawn)
				break
				
	