extends AudioStreamPlayer

func _physics_process(delta):
	var scene = get_tree().get_current_scene().get_name()
	if not (scene == "madeby" || scene == "mainmenu"):
		stop()
	else:
		if not is_playing():
			play()